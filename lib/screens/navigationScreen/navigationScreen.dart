import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:har/bloc/sensorData_bloc.dart';
import 'package:har/screens/classifyActivityScreen/classifyActivityScreen.dart';
import 'package:har/screens/recordSensorScreen/recordSensorScreen.dart';

/// Controls the displayed screen on the phone.
class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  /// The index of the current screen
  int _index = 0;

  /// Bloc that holds all information for the recordSensorScreen and this class
  final _bloc = SensorDataBloc();

  @override
  Widget build(BuildContext context) {
    // is used to hold the screen that will be displayed.
    Widget _child;
    // Selects the screen according to the _index
    switch (_index) {
      case 0:
        _child = RecordSensorScreen(_bloc);
        break;
      case 1:
        _child = ClassifyActivityScreen();
        break;
    }
    // Holds the information if the RecordSensorScreen is recording.
    bool isRecording = false;
    // Subscribes to the information if the RecordSensorScreen is recording.
    // and changes isRecording accordingly.
    _bloc.isRecording.listen((data) {
      isRecording = data;
    });
    return Scaffold(
      body: SizedBox.expand(child: _child),
      // The navigation bar that controls the screen changing
      bottomNavigationBar: BottomNavigationBar(
        onTap: (newIndex) => {
          // When no data is recorded, the screen can be changed.
          if (isRecording == false)
            {
              setState(() => _index = newIndex),
            }
          else
            // When data is recorded display info message.
            {
              Flushbar<dynamic>(
                title: "Can't classify",
                message: "Can't classify, while recording!",
                duration: Duration(seconds: 3),
              )..show(context),
            }
        },
        currentIndex: _index,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.save), title: Text("Record")),
          BottomNavigationBarItem(
              icon: Icon(Icons.local_activity), title: Text("Classify")),
        ],
      ),
    );
  }
}
