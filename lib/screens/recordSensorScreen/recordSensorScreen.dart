import 'dart:async';
import 'package:flutter/material.dart';
import 'package:har/bloc/sensorData_bloc.dart';
import 'package:har/bloc/sensorData_event.dart';
import 'package:har/screens/recordSensorScreen/components/sensorCard.dart';
import 'package:sensors/sensors.dart';

/// Bloc that holds all information for this class
SensorDataBloc _bloc;

/// A screen that controls the recording of sensor data. It also displays the
/// current sensor information.
class RecordSensorScreen extends StatefulWidget {
  RecordSensorScreen(SensorDataBloc bloc) {
    _bloc = bloc;
  }

  @override
  _RecordSensorScreenState createState() => _RecordSensorScreenState();
}

/// The different Activities that can be recorded.
enum Activities { Test, Running, Biking, Hiking }

class _RecordSensorScreenState extends State<RecordSensorScreen> {
  /// Holds the stream subscriptions for the sensors.
  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Record Activity'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // Displays the current accelerometer values
          StreamBuilder(
            stream: _bloc.accelerometerValues,
            initialData: [0.0, 0.0, 0.0],
            builder:
                (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
              return SensorCard("Accelerometer", snapshot.data);
            },
          ),
          // Displays the current gyroscope values.
          StreamBuilder(
            stream: _bloc.gyroscopeValues,
            initialData: [0.0, 0.0, 0.0],
            builder:
                (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
              return SensorCard("Gysroscope", snapshot.data);
            },
          ),
          // Displays the current userAccelerometer values.
          StreamBuilder(
            stream: _bloc.userAccelerometerValues,
            initialData: [0.0, 0.0, 0.0],
            builder:
                (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
              return SensorCard("UserAccelerometer", snapshot.data);
            },
          ),
          // The selection for the current activity.
          StreamBuilder(
            stream: _bloc.selectedActivityIndex,
            initialData: 0,
            builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
              return ListTile(
                title: Text("Activity Selection\nCurrent: " +
                    Activities.values[snapshot.data]
                        .toString()
                        .split(".")
                        .last),
                trailing: PopupMenuButton<Activities>(
                  onSelected: (Activities result) async {
                    _bloc.sensorDataEventSink
                        .add(SelectActivityEvent(result.index, context));
                  },
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<Activities>>[
                    const PopupMenuItem<Activities>(
                      value: Activities.Test,
                      //Does't need to be translated, because native words should be used
                      child: Text('Test'),
                    ),
                    const PopupMenuItem<Activities>(
                      value: Activities.Biking,
                      //Does't need to be translated, because native words should be used
                      child: Text('Biking'),
                    ),
                    const PopupMenuItem<Activities>(
                      value: Activities.Running,
                      //Does't need to be translated, because native words should be used
                      child: Text('Running'),
                    ),
                    const PopupMenuItem<Activities>(
                      value: Activities.Hiking,
                      //Does't need to be translated, because native words should be used
                      child: Text('Hiking'),
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
      // The Button that controls the recording of sensor data.
      floatingActionButton: StreamBuilder(
        stream: _bloc.isRecording,
        initialData: false,
        builder: (BuildContext context, AsyncSnapshot<bool> isRecording) {
          return FloatingActionButton(
            child: isRecording.data ? Icon(Icons.stop) : Icon(Icons.play_arrow),
            elevation: 20,
            tooltip: "Start recording",
            onPressed: () {
              if (isRecording.data) {
                _bloc.sensorDataEventSink.add(CloseSenorDataSinkEvent());
              } else {
                _bloc.sensorDataEventSink.add(OpenSensorDataSinkEvent());
              }
            },
          );
        },
      ),
    );
  }

  @override

  /// Cancels all sensor subscriptions
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }

  @override

  /// Is called on first screen creation
  ///
  /// Subscribes to the sensor streams. On every accelerometer event
  /// the sensor data will be saved.
  void initState() {
    super.initState();
    _streamSubscriptions
        .add(accelerometerEvents.listen((AccelerometerEvent event) {
      _bloc.sensorDataEventSink
          .add(AddAccelerometerDataEvent(event.x, event.y, event.z));
      _bloc.sensorDataEventSink.add(
        WriteToSensorDataSinkEvent(),
      );
    }));
    _streamSubscriptions.add(gyroscopeEvents.listen((GyroscopeEvent event) {
      _bloc.sensorDataEventSink
          .add(AddGyroscopeDataEvent(event.x, event.y, event.z));
    }));
    _streamSubscriptions
        .add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      _bloc.sensorDataEventSink
          .add(AddUserAccelerometerDataEvent(event.x, event.y, event.z));
    }));
  }
}
