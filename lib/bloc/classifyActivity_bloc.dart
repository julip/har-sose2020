import 'dart:async';
import 'package:har/screens/classifyActivityScreen/components/lineChart.dart';
import 'package:tflite_flutter/tflite_flutter.dart';
import 'classifyActivity_event.dart';
import 'package:charts_flutter/flutter.dart' as charts;

/// Holds all data for the classifyActivityScreen.
class ClassifyActivityBloc {
  /// Holds the accelerometer values.
  List<double> _accelerometerValues = List<double>();

  /// Holds the gyroscope values.
  List<double> _gyroscopeValues;

  /// Holds the userAccelerometer values.
  List<double> _userAccelerometerValues;

  /// The interpreter, that classifies the sensor data.
  Interpreter _interpreter;

  /// counts the amount of sensor data recorded. When 30 it will be classified.
  int _count = 0;

  /// The count of already classified sensor data.
  int _classificationCount = 0;

  /// The probabilities that the activity is hiking, biking or running
  List<int> _classificationProbabilities = [0, 0, 0];

  /// Holds the sensor data of the activity till there is enough to classify it.
  List _activityData = List<dynamic>();

  /// Holds the probability that the classifications result is hiking
  List<LinearClassification> _hikingData = List();

  /// Holds the probability that the classifications result is biking
  List<LinearClassification> _bikingData = List();

  /// Holds the probability that the classifications result is running
  List<LinearClassification> _runningData = List();

  ///////////////////////Start of BLOC subscription handling\\\\\\\\\\\\\\\\\\\\
  /// Handel's the state of [_accelerometerValues].
  final _accelerometerValuesStateController =
      StreamController<List<double>>.broadcast();

  /// Sink for incoming [_accelerometerValues] changes.
  StreamSink<List<double>> get _inAccelerometerValues =>
      _accelerometerValuesStateController.sink;

  /// Stream to notify subscribers to [_accelerometerValues].
  Stream<List<double>> get accelerometerValues =>
      _accelerometerValuesStateController.stream;

  /// Handel's the state of [_gyroscopeValues].
  final _gyroscopeValuesStateController =
      StreamController<List<double>>.broadcast();

  /// Sink for incoming [_gyroscopeValues] changes.
  StreamSink<List<double>> get _inGyroscopeValues =>
      _gyroscopeValuesStateController.sink;

  /// Stream to notify subscribers to [_gyroscopeValues].
  Stream<List<double>> get gyroscopeValues =>
      _gyroscopeValuesStateController.stream;

  /// Handel's the state of [_userAccelerometerValues].
  final _userAccelerometerValuesStateController =
      StreamController<List<double>>.broadcast();

  /// Sink for incoming [_userAccelerometerValues] changes.
  StreamSink<List<double>> get _inUserAccelerometerValues =>
      _userAccelerometerValuesStateController.sink;

  /// Stream to notify subscribers to [_userAccelerometerValues].
  Stream<List<double>> get userAccelerometerValues =>
      _userAccelerometerValuesStateController.stream;

  /// Handel's the state of chart.
  final _classificationChartStateController = StreamController<LineChart>();

  /// Sink for incoming chart changes.
  StreamSink<LineChart> get _inClassificationChart =>
      _classificationChartStateController.sink;

  /// Stream to notify subscribers to the chart.
  Stream<LineChart> get classificationChart =>
      _classificationChartStateController.stream;

  /// Handles all classify activity events.
  final _classifyActivityEventController =
      StreamController<ClassifyActivityEvent>();

  /// Stream to notify subscribers to all classify sensor events.
  Sink<ClassifyActivityEvent> get classifyActivityDataEventSink =>
      _classifyActivityEventController.sink;

  ///////////////////////END of BLOC subscription handling\\\\\\\\\\\\\\\\\\\\\\

  /// Creates the bloc, that listens to all classify activity events and maps then
  /// to the current state.
  ClassifyActivityBloc() {
    //Whenever there is a new event, we want to map it to a new state
    _classifyActivityEventController.stream.listen(_mapEventToState);
  }

  /// maps all [event]'s to the state of the bloc.
  void _mapEventToState(ClassifyActivityEvent event) {
    if (event is AddAccelerometerDataEvent) {
      _addAccelerometerData(event.x, event.y, event.z);
    } else if (event is AddGyroscopeDataEvent) {
      _addGyroscopeData(event.x, event.y, event.z);
    } else if (event is AddUserAccelerometerDataEvent) {
      _addUserAccelerometerData(event.x, event.y, event.z);
    } else if (event is InitializeInterpreterEvent) {
      _loadModel();
    }
  }

  /// Cancels all subscriptions to state changes.
  void dispose() {
    _classifyActivityEventController.close();
    _accelerometerValuesStateController.close();
    _gyroscopeValuesStateController.close();
    _userAccelerometerValuesStateController.close();
    _classificationChartStateController.close();
  }

  /// Adds the current accelerometer values to the [_gyroscopeValues].
  ///
  /// Takes the [x], [y] and [z] axes of the gyroscope.
  /// When [_count] reaches 30 the the data while be classified.
  void _addAccelerometerData(double x, double y, double z) {
    _accelerometerValues = <double>[x, y, z];
    _inAccelerometerValues.add(_accelerometerValues);
    _count++;
    _saveActivityData();
    if (_count == 30) {
      _classify();
      _mapClassificationToSeriesList();
      _count = 0;
    }
  }

  /// Adds the current gyroscope values to the [_gyroscopeValues].
  ///
  /// Takes the [x], [y] and [z] axes of the gyroscope.
  void _addGyroscopeData(double x, double y, double z) {
    _gyroscopeValues = <double>[x, y, z];
    _inGyroscopeValues.add(_gyroscopeValues);
  }

  /// Adds the current userAccelerometer values to the [_userAccelerometerValues].
  ///
  /// Takes the [x], [y] and [z] axes of the userAccelerometer.
  void _addUserAccelerometerData(double x, double y, double z) {
    _userAccelerometerValues = <double>[x, y, z];
    _inUserAccelerometerValues.add(_userAccelerometerValues);
  }

  /// Save the [_accelerometerValues] in an [_interpreter] friendly format.
  void _saveActivityData() {
    double accX = _accelerometerValues[0];
    double accY = _accelerometerValues[1];
    double accZ = _accelerometerValues[2];
    // When the user sensors are also used for classification this must be
    // commented in
    /*double gyrX = _gyroscopeValues[0];
    double gyrY = _gyroscopeValues[1];
    double gyrZ = _gyroscopeValues[2];
    double userAccX = _userAccelerometerValues[0];
    double userAccY = _userAccelerometerValues[1];
    double userAccZ = _userAccelerometerValues[2];*/

    if (_activityData.isEmpty) {
      _activityData.add(List<dynamic>());
    }
    List dummy = List<dynamic>();
    dummy.add([accX]);
    dummy.add([accY]);
    dummy.add([accZ]);
    _activityData[0].add(dummy);
  }

  /// Classify the [_activityData]. The probabilities for each activity
  /// will be saved in the [_classificationProbabilities] list.
  void _classify() {
    var output = [
      [0.0, 0.0, 0.0]
    ];
    _interpreter.run(_activityData, output);
    _classificationProbabilities[0] = (output[0][0] * 100).toInt();
    _classificationProbabilities[1] = (output[0][1] * 100).toInt();
    _classificationProbabilities[2] = (output[0][2] * 100).toInt();
    _activityData = List<dynamic>();
  }

  /// Loads the [_interpreter] and allocates all Tensor cores.
  void _loadModel() async {
    _interpreter = await Interpreter.fromAsset('harTrained.tflite');
    _interpreter.allocateTensors();
  }

  /// Maps the [_classificationProbabilities] to a format that can be used
  /// in the Chart.
  void _mapClassificationToSeriesList() {
    _hikingData.add(LinearClassification(
        _classificationCount, _classificationProbabilities[0]));

    _bikingData.add(LinearClassification(
        _classificationCount, _classificationProbabilities[1]));

    _runningData.add(LinearClassification(
        _classificationCount, _classificationProbabilities[2]));
    _classificationCount++;
    var _seriesList = [
      charts.Series<LinearClassification, int>(
        id: 'Hiking',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (LinearClassification hiking, _) =>
            hiking.classificationIntervalCount,
        measureFn: (LinearClassification hiking, _) => hiking.probability,
        data: _hikingData,
      ),
      charts.Series<LinearClassification, int>(
        id: 'Biking',
        colorFn: (_, __) => charts.MaterialPalette.yellow.shadeDefault,
        domainFn: (LinearClassification biking, _) =>
            biking.classificationIntervalCount,
        measureFn: (LinearClassification biking, _) => biking.probability,
        data: _bikingData,
      ),
      charts.Series<LinearClassification, int>(
        id: 'Running',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearClassification running, _) =>
            running.classificationIntervalCount,
        measureFn: (LinearClassification running, _) => running.probability,
        data: _runningData,
      ),
    ];

    if (_classificationCount == 100) {
      _classificationCount = 0;
      _hikingData = List();
      _bikingData = List();
      _runningData = List();
    }

    _inClassificationChart.add(LineChart(_seriesList, animate: true));
  }
}

/// Used to store Classification information for the chart.
class LinearClassification {
  /// Count of the current  classification interval.
  final int classificationIntervalCount;

  /// The probability that the data belongs to the activity.
  final int probability;

  /// Used to store Classification information for the chart.
  ///
  /// Takes the [classificationIntervalCount] and the [probability].
  LinearClassification(this.classificationIntervalCount, this.probability);
}
