import 'dart:async';
import 'dart:io';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:har/screens/recordSensorScreen/recordSensorScreen.dart'
    as sensor_screen;
import 'sensorData_event.dart';

/// Holds all data for the recordSensorScreen.
class SensorDataBloc {
  /// Holds the accelerometer values.
  List<double> _accelerometerValues = List<double>();

  /// Holds the gyroscope values.
  List<double> _gyroscopeValues;

  /// Holds the userAccelerometer values.
  List<double> _userAccelerometerValues;

  /// Is currently recording data ?
  bool _isRecording = false;

  /// Csv file where the sensor data is saved to.
  File _file;

  /// IOSink that rides data to the [_file] when new data arrives.
  IOSink _sink;

  /// The current selected activity.
  int _selectedActivityIndex = 0;

  /// The time of recording the data.
  int _timeOfRecording;

  ///////////////////////Start of BLOC subscription handling\\\\\\\\\\\\\\\\\\\\
  /// Handel's the state of [_accelerometerValues].
  final _accelerometerValuesStateController =
      StreamController<List<double>>.broadcast();

  /// Sink for incoming [_accelerometerValues] changes.
  StreamSink<List<double>> get _inAccelerometerValues =>
      _accelerometerValuesStateController.sink;

  /// Stream to notify subscribers to [_accelerometerValues].
  Stream<List<double>> get accelerometerValues =>
      _accelerometerValuesStateController.stream;

  /// Handel's the state of [_gyroscopeValues].
  final _gyroscopeValuesStateController =
      StreamController<List<double>>.broadcast();

  /// Sink for incoming [_gyroscopeValues] changes.
  StreamSink<List<double>> get _inGyroscopeValues =>
      _gyroscopeValuesStateController.sink;

  /// Stream to notify subscribers to [_gyroscopeValues].
  Stream<List<double>> get gyroscopeValues =>
      _gyroscopeValuesStateController.stream;

  /// Handel's the state of [_userAccelerometerValues].
  final _userAccelerometerValuesStateController =
      StreamController<List<double>>.broadcast();

  /// Sink for incoming [_userAccelerometerValues] changes.
  StreamSink<List<double>> get _inUserAccelerometerValues =>
      _userAccelerometerValuesStateController.sink;

  /// Stream to notify subscribers to [_userAccelerometerValues].
  Stream<List<double>> get userAccelerometerValues =>
      _userAccelerometerValuesStateController.stream;

  /// Handel's the state of [_isRecording].
  final _isRecordingStateController = StreamController<bool>.broadcast();

  /// Sink for incoming [_isRecording] changes.
  StreamSink<bool> get _inIsRecording => _isRecordingStateController.sink;

  /// Stream to notify subscribers to [_isRecording].
  Stream<bool> get isRecording => _isRecordingStateController.stream;

  /// Handel's the state of [_selectedActivityIndex].
  final _selectedActivityIndexStateController =
      StreamController<int>.broadcast();

  /// Sink for incoming [_selectedActivityIndex] changes.
  StreamSink<int> get _inSelectedActivityIndexSink =>
      _selectedActivityIndexStateController.sink;

  /// Stream to notify subscribers to [_selectedActivityIndex].
  Stream<int> get selectedActivityIndex =>
      _selectedActivityIndexStateController.stream;

  /// Handles all sensor data events.
  final _sensorDataEventController = StreamController<SensorDataEvent>();

  /// Stream to notify subscribers to all sensor data events.
  Sink<SensorDataEvent> get sensorDataEventSink =>
      _sensorDataEventController.sink;

  ///////////////////////END of BLOC subscription handling\\\\\\\\\\\\\\\\\\\\\\

  /// Creates the bloc, that listens to all sensor data events and maps then
  /// to the current state.
  SensorDataBloc() {
    //Whenever there is a new event, we map it to a new state
    _sensorDataEventController.stream.listen(_mapEventToState);
  }

  /// maps all [event]'s to the state of the bloc.
  void _mapEventToState(SensorDataEvent event) {
    if (event is AddAccelerometerDataEvent) {
      _addAccelerometerData(event.x, event.y, event.z);
    } else if (event is AddGyroscopeDataEvent) {
      _addGyroscopeData(event.x, event.y, event.z);
    } else if (event is AddUserAccelerometerDataEvent) {
      _addUserAccelerometerData(event.x, event.y, event.z);
    } else if (event is OpenSensorDataSinkEvent) {
      _openSensorDataSink();
    } else if (event is CloseSenorDataSinkEvent) {
      _closeSenorDataSinkEvent();
    } else if (event is WriteToSensorDataSinkEvent) {
      _writeToSensorDataSinkEvent();
    } else if (event is SelectActivityEvent) {
      _selectActivity(event.activityIndex, event.context);
    }
  }

  /// Cancels all subscriptions to state changes.
  void dispose() {
    _sensorDataEventController.close();
    _accelerometerValuesStateController.close();
    _gyroscopeValuesStateController.close();
    _userAccelerometerValuesStateController.close();
    _isRecordingStateController.close();
  }

  /// Adds the current accelerometer values to the [_accelerometerValues].
  ///
  /// Takes the [x], [y] and [z] axes of the accelerometer.
  void _addAccelerometerData(double x, double y, double z) {
    _accelerometerValues = <double>[x, y, z];
    _inAccelerometerValues.add(_accelerometerValues);
  }

  /// Adds the current gyroscope values to the [_gyroscopeValues].
  ///
  /// Takes the [x], [y] and [z] axes of the gyroscope.
  void _addGyroscopeData(double x, double y, double z) {
    _gyroscopeValues = <double>[x, y, z];
    _inGyroscopeValues.add(_gyroscopeValues);
  }

  /// Adds the current userAccelerometer values to the [_userAccelerometerValues].
  ///
  /// Takes the [x], [y] and [z] axes of the userAccelerometer.
  void _addUserAccelerometerData(double x, double y, double z) {
    _userAccelerometerValues = <double>[x, y, z];
    _inUserAccelerometerValues.add(_userAccelerometerValues);
  }

  /// Flips the recording option of data.
  ///
  ///  true => false
  ///  false => true
  void _changeDataRecordingOption() {
    _isRecording = !_isRecording;
    _inIsRecording.add(_isRecording);
  }

  /// Opens the [_sink] and creates a [_file] to save data to.
  ///
  /// [_file] name is created from activity name and the time of the [_file]
  /// creation.
  void _openSensorDataSink() async {
    DateTime dateTime = DateTime.now();
    String dateString = dateTime.day.toString() +
        "-" +
        dateTime.month.toString() +
        "-" +
        dateTime.year.toString() +
        "-" +
        dateTime.hour.toString() +
        "-" +
        dateTime.minute.toString();
    String activityName = sensor_screen
        .Activities.values[_selectedActivityIndex]
        .toString()
        .split(".")
        .last
        .toLowerCase();
    final Directory directory = await getExternalStorageDirectory();
    _file = File('${directory.path}/$activityName-$dateString.csv');
    _sink = _file.openWrite();
    _changeDataRecordingOption();
  }

  /// Closes the [_sink].
  void _closeSenorDataSinkEvent() {
    _changeDataRecordingOption();
    _sink.close();
    _sink = null;
  }

  /// When [_isRecording] == true all sensor data is saved, throw the [_sink]
  /// to the [_file].
  ///
  /// Data saving format is
  /// "activity,accX,accY,accZ,gyrX,gyrY,gyrZ,userAccX,userAccY,userAccZ,time"
  void _writeToSensorDataSinkEvent() {
    if (_isRecording) {
      String activityName = sensor_screen
          .Activities.values[_selectedActivityIndex]
          .toString()
          .split(".")
          .last
          .toLowerCase();
      _timeOfRecording = DateTime.now().millisecondsSinceEpoch;

      if (_sink != null) {
        _sink.write("$activityName," +
            _accelerometerValues[0].toString() +
            "," +
            _accelerometerValues[1].toString() +
            "," +
            _accelerometerValues[2].toString() +
            "," +
            _gyroscopeValues[0].toString() +
            "," +
            _gyroscopeValues[1].toString() +
            "," +
            _gyroscopeValues[2].toString() +
            "," +
            _userAccelerometerValues[0].toString() +
            "," +
            _userAccelerometerValues[1].toString() +
            "," +
            _userAccelerometerValues[2].toString() +
            ",$_timeOfRecording\n");
      }
    }
  }

  /// Change the [_selectedActivityIndex] to the new [selectedActivityIndex].
  /// Takes also the current build [context] to display info messages.
  ///
  /// Changes the activity when [_isRecording] is false, else a info message is
  /// displayed that the activity could not be changed.
  void _selectActivity(int selectedActivityIndex, BuildContext context) {
    // If not recording change activity index
    if (!_isRecording) {
      this._selectedActivityIndex = selectedActivityIndex;
      _inSelectedActivityIndexSink.add(selectedActivityIndex);
    } else {
      Flushbar<dynamic>(
        title: "Can't change activity",
        message: "Can't change activity, while recording!",
        duration: Duration(seconds: 3),
      )..show(context);
    }
  }
}
