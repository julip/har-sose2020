import 'package:flutter/material.dart';

/// All events that are used for handling and saving sensor data.
abstract class SensorDataEvent {}

/// Event that takes the current accelerometer value and saves it.
class AddAccelerometerDataEvent extends SensorDataEvent {
  /// x axes of the sensor.
  double x;

  /// y axes of the sensor.
  double y;

  /// z axes of the sensor.
  double z;

  /// Event that takes the current accelerometer value and saves it.
  ///
  /// Takes the value for the [x], [y], [z] axes from the sensor.
  AddAccelerometerDataEvent(this.x, this.y, this.z);
}

/// Event that takes the current gyroscope value and saves it.
class AddGyroscopeDataEvent extends SensorDataEvent {
  /// x axes of the sensor.
  double x;

  /// y axes of the sensor.
  double y;

  /// z axes of the sensor.
  double z;

  /// Event that takes the current gyroscope value and saves it.
  ///
  /// Takes the value for the [x], [y], [z] axes from the sensor.
  AddGyroscopeDataEvent(this.x, this.y, this.z);
}

/// Event that takes the current userAccelerometer value and saves it.
class AddUserAccelerometerDataEvent extends SensorDataEvent {
  /// x axes of the sensor.
  double x;

  /// y axes of the sensor.
  double y;

  /// z axes of the sensor.
  double z;

  /// Event that takes the current userAccelerometer value and saves it.
  ///
  /// Takes the value for the [x], [y], [z] axes from the sensor.
  AddUserAccelerometerDataEvent(this.x, this.y, this.z);
}

/// Opens the sink that is used to save the sensor data to a csv file.
class OpenSensorDataSinkEvent extends SensorDataEvent {}

/// Closes the sink that is used to save the sensor data to a csv file.
class CloseSenorDataSinkEvent extends SensorDataEvent {}

/// Is called to save the current accelerometer, gyroscope and userAccelerometer
/// values to a csv file.
class WriteToSensorDataSinkEvent extends SensorDataEvent {
  WriteToSensorDataSinkEvent();
}

/// Select a new activity for recording.
class SelectActivityEvent extends SensorDataEvent {
  /// The index of the activity
  int activityIndex;

  /// The build context of the current screen.
  /// Used to display information messages
  BuildContext context;

  /// Select a new activity[activityIndex] for recording. The current [context]
  /// to display info messages.
  SelectActivityEvent(this.activityIndex, this.context);
}
