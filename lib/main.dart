import 'package:flutter/material.dart';
import 'package:har/screens/navigationScreen/navigationScreen.dart';

/// Entry point  for the app
void main() {
  runApp(MyApp());
}

/// Creates a material app with the navigationScreen as home screen.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          /*dark
        primarySwatch: Colors.grey,
        primaryColor: Colors.black,
        brightness: Brightness.dark,
        backgroundColor: const Color(0xFF212121),
        accentColor: Colors.white,
        accentIconTheme: IconThemeData(color: Colors.black),
        dividerColor: Colors.black12,
        */

          ),
      home: NavigationScreen(),
    );
  }
}
