/// All events that are used for handling and classifying activities.
abstract class ClassifyActivityEvent {}

/// Event that takes the current accelerometer.
class AddAccelerometerDataEvent extends ClassifyActivityEvent {
  /// x axes of the sensor.
  double x;

  /// y axes of the sensor.
  double y;

  /// z axes of the sensor.
  double z;

  /// Event that takes the current accelerometer value.
  ///
  /// Takes the value for the [x], [y], [z] axes from the sensor.
  AddAccelerometerDataEvent(this.x, this.y, this.z);
}

/// Event that takes the current gyroscope value.
class AddGyroscopeDataEvent extends ClassifyActivityEvent {
  /// x axes of the sensor.
  double x;

  /// y axes of the sensor.
  double y;

  /// z axes of the sensor.
  double z;

  /// Event that takes the current gyroscope value.
  ///
  /// Takes the value for the [x], [y], [z] axes from the sensor.
  AddGyroscopeDataEvent(this.x, this.y, this.z);
}

/// Event that takes the current userAccelerometer.
class AddUserAccelerometerDataEvent extends ClassifyActivityEvent {
  /// x axes of the sensor.
  double x;

  /// y axes of the sensor.
  double y;

  /// z axes of the sensor.
  double z;

  /// Event that takes the current userAccelerometer.
  ///
  /// Takes the value for the [x], [y], [z] axes from the sensor.
  AddUserAccelerometerDataEvent(this.x, this.y, this.z);
}

/// Event that initializes the interpreter and load all tensor cores.
class InitializeInterpreterEvent extends ClassifyActivityEvent {}
