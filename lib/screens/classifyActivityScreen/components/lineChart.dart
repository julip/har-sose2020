import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:har/bloc/classifyActivity_bloc.dart';

/// Chart that displays the classification results.
class LineChart extends StatelessWidget {
  /// Holds the classification results in a form where the charts
  /// add on can make sense out of it.
  final List<charts.Series> seriesList;

  /// Should the chart be animated.
  final bool animate;

  /// Chart that displays the classification results.
  ///
  /// Takes the classification results [seriesList] and if the results should
  /// be [animate].
  LineChart(this.seriesList, {this.animate});

  /// Creates a [LineChart] with sample data and no transition.
  factory LineChart.Init() {
    return LineChart(
      _createInitData(),
      animate: false,
    );
  }

  /// Create init data, that is used to display the chart before the first
  /// classification.
  static List<charts.Series<LinearClassification, int>> _createInitData() {
    return [
      charts.Series<LinearClassification, int>(
        id: 'RecordingData',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (LinearClassification dummy, _) =>
            dummy.classificationIntervalCount,
        measureFn: (LinearClassification dummy, _) => dummy.probability,
        data: [LinearClassification(0, 0)],
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    // Creates the chart with the classification data
    return charts.LineChart(
      seriesList,
      defaultRenderer:
          charts.LineRendererConfig(includeArea: true, stacked: false),
      animate: animate,
      behaviors: [
        charts.SeriesLegend(
          defaultHiddenSeries: ['RecordingData'],
        )
      ],
    );
  }
}
