import 'package:flutter/material.dart';

/// Widget that is used to display all data from a sensor.
class SensorCard extends StatelessWidget {
  /// The name this sensor
  final String _sensorName;

  /// The x,y,z data from this sensor.
  final List<double> _sensorData;

  /// Takes the [_sensorName] and corresponding [_sensorData]
  SensorCard(this._sensorName, this._sensorData);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
        vertical: 2,
        horizontal: 2,
      ),
      elevation: 7,
      child: Container(
        child: ListTile(
          title: Text(_sensorName),
          subtitle: Text("x: " +
              _sensorData[0].toString() +
              "\n" +
              "y: " +
              _sensorData[1].toString() +
              "\n" +
              "z: " +
              _sensorData[2].toString()),
        ),
      ),
    );
  }
}
