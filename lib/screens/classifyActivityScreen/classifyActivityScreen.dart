import 'dart:async';
import 'package:flutter/material.dart';
import 'package:har/bloc/classifyActivity_bloc.dart';
import 'package:har/bloc/classifyActivity_event.dart';
import 'package:har/screens/classifyActivityScreen/components/lineChart.dart';
import 'package:har/screens/recordSensorScreen/components/sensorCard.dart';
import 'package:sensors/sensors.dart';

/// A screen that classifies the sensor data
class ClassifyActivityScreen extends StatefulWidget {
  ClassifyActivityScreen();

  @override
  _ClassifyActivityScreenState createState() => _ClassifyActivityScreenState();
}

class _ClassifyActivityScreenState extends State<ClassifyActivityScreen> {
  /// Bloc that holds all information for this class
  final _bloc = ClassifyActivityBloc();

  /// Holds the stream subscriptions for the sensors.
  List<StreamSubscription<dynamic>> _streamSubscriptions =
      <StreamSubscription<dynamic>>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Classify Activity'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          StreamBuilder(
            // Displays the current accelerometer values
            stream: _bloc.accelerometerValues,
            initialData: [0.0, 0.0, 0.0],
            builder:
                (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
              return SensorCard("Accelerometer", snapshot.data);
            },
          ),
          // Displays the current gyroscope values
          StreamBuilder(
            stream: _bloc.gyroscopeValues,
            initialData: [0.0, 0.0, 0.0],
            builder:
                (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
              return SensorCard("Gysroscope", snapshot.data);
            },
          ),
          // Displays the current userAccelerometer values
          StreamBuilder(
            stream: _bloc.userAccelerometerValues,
            initialData: [0.0, 0.0, 0.0],
            builder:
                (BuildContext context, AsyncSnapshot<List<double>> snapshot) {
              return SensorCard("UserAccelerometer", snapshot.data);
            },
          ),
          // A chart that holds all classification results over time
          Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            child: StreamBuilder(
              stream: _bloc.classificationChart,
              initialData: LineChart.Init(),
              builder:
                  (BuildContext context, AsyncSnapshot<LineChart> snapshot) {
                return snapshot.data;
              },
            ),
          ),
        ],
      ),
    );
  }

  @override

  /// Cancels all sensor subscriptions
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }

  @override

  /// Is called on first screen creation
  ///
  /// Subscribes to the sensor streams and initialises the interpreter.
  void initState() {
    super.initState();
    _streamSubscriptions
        .add(accelerometerEvents.listen((AccelerometerEvent event) {
      _bloc.classifyActivityDataEventSink
          .add(AddAccelerometerDataEvent(event.x, event.y, event.z));
    }));
    _streamSubscriptions.add(gyroscopeEvents.listen((GyroscopeEvent event) {
      _bloc.classifyActivityDataEventSink
          .add(AddGyroscopeDataEvent(event.x, event.y, event.z));
    }));
    _streamSubscriptions
        .add(userAccelerometerEvents.listen((UserAccelerometerEvent event) {
      _bloc.classifyActivityDataEventSink
          .add(AddUserAccelerometerDataEvent(event.x, event.y, event.z));
    }));
    _bloc.classifyActivityDataEventSink.add(InitializeInterpreterEvent());
  }
}
